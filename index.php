<?php
require_once "db/connect.php";
require_once "process/login_process.php";
?>


<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Form Login</title>

<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link href="assets/css/datepicker3.css" rel="stylesheet">
<link href="assets/css/styles.css" rel="stylesheet">

</head>

<body>
    
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1 col-ssm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading" align="center">FORM LOGIN</div>
                <div class="panel-body">
                    <!-- VALIDASI -->
                    <?require_once "view/validasi.php";?>
                    <!-- END VALIDASI -->
                    <form action="" method="POST" role="form">
                        <fieldset>

                            <div class="form-group"><label>Login Sebagai :</label>
                                <select name="sebagai" class="form-control">
                                    <option value="wali">MENTOR</option>
                                    <option value="siswa">SANTRI</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <input class="form-control" placeholder="Username" name="username" type="text" autofocus="" required="">
                            </div>

                            <div class="form-group">
                                <input class="form-control" placeholder="Password" name="password" type="password" value="" required="">
                            </div>

                            <button name="submit" type="submit" class="btn btn-primary btn-block btn-flat" >Masuk</button>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div><!-- /.col-->
    </div><!-- /.row -->    
    
</body>
</html>
