<?
include "../db/connect.php";
include_once "templates/header.php";
include_once "templates/sidebar.php";



?>
<script type="text/javascript" src="webcam.js"></script>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">  
        
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
            <li class="active">Forms</li>
        </ol>
    </div><p>

        <div class="row">
        <div class="col-lg-12">
               <div id="demo">
    <div class="step-app">
      <ul class="step-steps">
        <li class="active"><a href="#"><span class="glyphicon glyphicon-time"></span><b> PAGI DIMULAI JAM 05.00 - 08.00 WIB</b></a></li>
        <li class="active"><a href="#"><span class="glyphicon glyphicon-time"></span><b> SIANG DIMULAI JAM 11.00 - 13.00 WIB</b></a></li>
        <li class="active"><a href="#"><span class="glyphicon glyphicon-time"></span><b> SORE DIMULAI JAM 14.00 - 16.00 WIB</b></a></li>
      </ul>
 
    </div>
  </div>
        </div>
        </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                 
<?
date_default_timezone_set('Asia/Jakarta');
$time     = date('G');

$jam = $time;
// pagi
$pagi=5;
$batas_pagi=7;
// siang
$siang=11;
$batas_siang=12;
// sore
$sore=14;
$batas_sore=15;

// LAPRAN PAGI
if ($pagi <= $jam && $jam <= $batas_pagi ) {
?>
    <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
        <span class="glyphicon glyphicon-plus"></span>Update Kegiatan Pagi
    </button>                
<?
// END LAPRAN PAGI

// LAPRAN SIANG
} else if ($siang <= $jam && $jam <= $batas_siang ) {
?>
    <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
        <span class="glyphicon glyphicon-plus"></span>Update Kegiatan Siang
    </button>
<?
// LAPORAN SORE

// LAPORAN SORE
} else if ($sore <= $jam && $jam <= $batas_sore ) {
?>
    <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
        <span class="glyphicon glyphicon-plus"></span>Update Kegiatan Sore
    </button>
<?
//END LAPORAN SORE

} else {
?>
    <b>UPDATE KEGIATAN BELUM DIBUKA </b>

<?
}
?>
</div>


                    <!-- data laporan -->
                    <?include_once "tampil/laporan_data.php";?>
                    <!-- end -->
                    <!-- form madal -->
                    <?include_once "tampil/laporan_form.php";?>
                    <!-- end -->  

            </div>
        </div>
    </div>
</div>

<?include_once "templates/footer.php";?>
<script>
    $('#demo').steps({
      onFinish: function () {
        alert('Wizard Completed');
      }
    });
  </script>
  <script src="http://code.jquery.com/jquery-latest.min.js"></script>