<?
include_once "templates/header.php";
include_once "templates/sidebar.php";
$query  =   mysqli_query($connect,"SELECT * FROM siswa WHERE username='$username'") or die (mysqli_error($sql));
$tampil =   mysqli_fetch_assoc($query);
$id_siswa = $tampil['id_siswa'];
?> 

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">  
        
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
            <li class="active">Index</li>
        </ol>
    </div><p>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading"><span class="glyphicon glyphicon-user"></span> Your Profile</div>
                    <div class="panel-body">
                                
                        <div class="row">
                            <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="images/avatar/avatar.png" width="200px" class="img-circle img-responsive"> </div>
                        
                        <div class=" col-md-9 col-lg-9 ">     
                            <!-- tampil profile -->
                            <?include_once "tampil/profile.php";?>
                            <!-- end -->

                            <a href="#myModal" id='custId' class="btn btn-primary" data-toggle="modal" data-id="<?echo$tampil['id_siswa'];?>"><span class="glyphicon glyphicon-edit"></span> Edit Your Profile</a>
                                  
                            <a href="#" class="btn btn-danger"><span class="glyphicon glyphicon-edit"></span> Change Password</a>

                            <!-- <a href='#myModal' class='btn btn-default btn-small' id='custId' data-toggle='modal' data-id=".$row['id'].">Detail</a> -->
                        </div>
                        </div>
                            <!-- edit profile -->
                             <?include_once "tampil/edit_profile.php";?>
                            <!-- end -->

                    </div>
                </div>
            </div>
        </div>
</div>
           


<?
include_once "templates/footer.php";
?>
<script type="text/javascript">
    $(document).ready(function(){
        $('#myModal').on('show.bs.modal', function (e) {
            var rowid = $(e.relatedTarget).data('id_siswa');
            //menggunakan fungsi ajax untuk pengambilan data
            $.ajax({
                type : 'post',
                url : 'edit_profile.php',
                data :  'rowid='+ rowid,
                success : function(data){
                $('.fetched-data').html(data);//menampilkan data ke dalam modal
                }
            });
         });
    });
  </script>