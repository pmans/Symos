<div class="col-md-6">
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Your Profile</h4>
                </div>

                    <div class="modal-body">                                               
                        <form action="laporan_process.php" role="form" method="post">

                        <div class="form-group">
                            <label>Kegiatan Hari Ini?</label>
                            <textarea name="kegiatan" class="form-control" rows="3" required=""></textarea>
                        </div>

                            <button type="submit" class="btn btn-danger btn-block btn-flat">Update Kegiatan</button>

                        </form>
            </div>
        </div>
    </div>
</div>